**LFS** (Large File Storage) позволяет решить недостатки Git при работе с большими файлами. Когда вы отправляете изменения в удалённый репозиторий,
GitLFS перехватывает LFS файлы и отправляет их на LFS сервера, оставляя в репозитории ссылку на сервер.Это означает, что размер вашего
репозитория не будет расти с огромной скоростью, т.к. все большие файлы будут хранится на выделенном сервере.
Важным является то, что вы можете работать с LFS файлами также, как и с обычными, используя те же команды GitBash.
Однако для получения больших возможностей следует использоваться LFS команды, которые будут описаны ниже.

#### Шаг 1. Установка LFS
После скачивания необходимо установить раширение. Для этого необходимо скачать установочник с сайта [github](https://git-lfs.github.com/).
После этого следует воспользоваться командой:

`git lfs install`

```bash
$ git lfs install
Git LFS initialized.
```

#### Шаг 2. Включение поддержки LFS в настройках репозитория

![Зайдите в репозиторий, затем в Settings, затем поставьте галочку Allow LFS](/img/LFS/1.png)

```bash
git lfs install
Git LFS initialized
```

#### Шаг 3. Клонирование репозитория
Вместо обычной команды `git clone` воспользуйтесь следующей:

`git lfs clone <Адрес репозитория>`

```bash
$ git lfs clone https://galimskiy@dev.awem.by/bitbucket/scm/mcbug/prealpha.git
Cloning into 'prealpha'...
remote: Counting objects: 706, done.
remote: Compressing objects: 100% (318/318), done.
remote: Total 706 (delta 391), reused 642 (delta 387)
Receiving objects: 100% (706/706), 286.19 KiB | 0 bytes/s, done.
Resolving deltas: 100% (391/391), done.

```

Данная команда будет выполняться быстрее, поскольку первоначально будет загружены обычные файлы,
а затем - LFS-файлы с использованием параллельной загрузки.

#### Шаг 4. Получение LFS файлов
Для получения LFS файлов из удалённого репозитория воспользуйтесь следующей командой:

`git lfs fetch <Адрес репозитория>`

```bash
$ git lfs fetch origin
Fetching master
```

#### Шаг 5. Получение изменений из удалённого репозитория
Если возникают какие-либо проблемы с получением данных из удалённого репозитория командной `git pull`, попробуйте заменить её на:

`git lfs pull`

```bash
$ git lfs pull
```

#### Шаг 6. Указание LFS-файлов
Для того, чтобы файл стал LFS, необходимо явно указать расширение файлов с помощью следующей команды:

`git lfs track "*.ogg"`

```bash
$ git lfs track "*.cs"
Tracking "*.cs"
```

После её выполнения все файлы с расширением **.ogg* будут LFS. Также будет создан файл *.gitattributes* с фильтрами.
Возможно задать следующие варианты отслеживания:

* Отслеживать все .ogg файлы в любом каталоге
`git lfs track "*.ogg"`

* Отслеживать файлы треков с именем music.ogg в любом каталоге
`git lfs track "music.ogg"`

* Отслеживать все файлы в каталоге Assets и во всех подкаталогах
`git lfs track "Assets/"`

* Отслеживать все файлы в каталоге Assets, но не в подкаталогах
`git lfs track "Assets/*`

* Отслеживать все файлы ogg в Assets / Audio
`git lfs track "Assets/Audio/*.ogg"`

* Отслеживать все файлы ogg в любом каталоге с именем Music
`git lfs track "**/Music/*.ogg"`

* Отслеживать png файлы, содержащие "xxhdpi" в их имени, в любом каталоге
`git lfs track "*xxhdpi*.png`

#### Шаг 7. Отправка изменений в удалённый репозиторий
Команда `git push` отправляет изменения как обычных, так и LFS файлов. После выполнения данной команды будет также выведен результат работы с LFS файлами:

`git push`

```bash
$ git push -u origin master
Git LFS: (133 of 133 files) 720.96 KB / 720.96 KB
Counting objects: 865, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (473/473), done.
Writing objects: 100% (865/865), 306.49 KiB | 0 bytes/s, done.
Total 865 (delta 413), reused 684 (delta 391)
Branch master set up to track remote branch master from origin.
To https://dev.awem.by/bitbucket/scm/test/galimskiy.git
 * [new branch]      master -> master
```

#### Шаг 8. Просмотр файлов, которые указаны как LFS
Для того, чтобы получить список файлов, которые определены как LFS воспользуйтесь следующей командой:

`git lfs track`

```bash
$ git lfs track
Listing tracked patterns
    *.cs (.gitattributes)
    *.jpg (.gitattributes)
    *.jpeg (.gitattributes)
    *.png (.gitattributes)
```

#### Шаг 9. Просмотр статуса LFS файлов

Для LFS файлов можно получить отдельную статистику с помощью команды:

```git lfs status```

```bash
$ git lfs status
On branch master
Git LFS objects to be pushed to origin/master:

Git LFS objects to be committed:

Git LFS objects not staged for commit:

open M3CBU_uprj/Assets/AwemFramework.meta: The system cannot find the file specified.
```

### <span style="color:blue"> <center>Выводы:</center> </span>
1. GIT LFS предназначена для работы с большими и бинарными файлами;
2. Работа с LFS-файлами аналогична работе с обычными файлами;
3. При работе с LFS-файлами можно пользоваться привычными командами Git, однако для получения дополнительных возможностей можно использовать LFS-команды.
