### Клонирование определённой ветки

#### Среда Git Bash
В случае необходимости получения из удалённого репозитория только определённой ветки
используется следующая команда:

`git clone -b <Ветка> --single-branch <Репозиторий>`

```sh
$ git clone -b master --single-branch https://galimskiy@dev.awem.by/bitbucket/scm/phf2p/ph-f2p.git
Cloning into 'ph-f2p'...
remote: Counting objects: 35827, done.
remote: Compressing objects: 100% (17016/17016), done.
remote: Total 35827 (delta 16465), reused 34547 (delta 16036)
Receiving objects: 100% (35827/35827), 2.28 GiB | 9.36 MiB/s, done.
Resolving deltas: 100% (16465/16465), done.
Checking out files: 100% (16598/16598), done.
```

Для того, чтобы убедиться в том, что скопирована только одна ветка воспользуемся командой
`git branch` с флагом *-a*, который предназначен для отображения всех веток,
в т.ч. и удаленных.

```bash
$ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
```

Если необходимо получить не всю историю, а только определённое количество последних коммитов
используется понятие глубины. **Глубина** (Depth) - это количество ревизий (коммитов),
которые будут отображаться в истории git. Команда, получающая последние
3 коммита из клонируемой ветки имеет следующий синтаксис:

`git clone --depth 3 -b <Ветка> --single-branch <Репозиторий>`

```bash
$ git clone --depth 3 -b release --single-branch https://galimskiy@dev.awem.by/bit
Cloning into 'ph-f2p'...
remote: Counting objects: 20807, done.
remote: Compressing objects: 100% (14451/14451), done.
remote: Total 20807 (delta 6157), reused 19051 (delta 5676)
Receiving objects: 100% (20807/20807), 1.20 GiB | 9.42 MiB/s, done.
Resolving deltas: 100% (6157/6157), done.
Checking out files: 100% (22210/22210), done.
```
Проверить количество полученных коммитов можно с помощью команды `git log`.

```bash
S git log --decorate --graph
*	commit 582d9679e00ca3S154e05b3e5862d3181984635f (HEAD -> release, origin/release)
| Author: Немыченко Александр <nemychenko@awem.local>
| Date:	Wed Jun 28 13:09:36 2017 +0300
|    issue: https://mogilev.awem.by/jira/browse/PH-2430 message: construction m odify dialog order fix
*	commit ld0805454e869793aa3484ddbf96068eaael2b71 Author: Немыченко Александр <nemychenko@awem.local>
| Date:	Wed Jun 28 13:09:36 2017 +0300
|    issue: https://mogilev.awem.by/jira/browse/PH-2430 message: construction modify dialog order fix
*	commit 62a5albdc7f5f012ad4bdl6c6dc03da3e69d841c (grafted)
  Author: Немыченко Александр <nemychenko@awem.local>
  Date:	Wed Jun 28 10:37:31 2017 +0300
      Merge branch 'develop’ into release
```

Если в дальнейшем возникнет необходимость изменения глубины нужно воспользоваться командой
**fetch**. Данная команда получает изменения с сервера и сохраняет их в каталог **refs/remotes/**.
Важно понимать, что полученные изменения никак не повлияют на локальные ветки до тех пор,
пока не будут слиты с текущими наработками.
Синтаксис данной команды выглядит следующим образом:

`git fetch <Адрес репозитория>`

Для получения определённого числа коммитов добавьте флаг *--depth* с указанием количества получаемых коммитов:

`git fetch --depth 10 <Адрес репозиторий> <Ветка>`

```bash
S git fetch --depth 10 origin release
remote: Counting objects: 5646, done.
remote: Compressing objects: 100% (3988/3988), done.
remote: Total 4402 (delta 1615), reused 2708 (delta 241)
Receiving objects: 100% (4402/4402), 468.55 Mi В | 3.54 MiB/s, done.
Resolving deltas: 100% (1615/1615), completed with 832 local objects.
From https://dev.awem.by/bitbucket/scm/phf2p/ph-f2p
 * branch	    release -> FETCH_HEAD
```

Затем нужно вмержить себе полученнные изменения. Это можно сделать двумя способами:
* `git merge <Репозиторий> <Ветка>`
С помощью данного способа полученные из удалённого репозитория изменения будут слиты с локальными.
В случае, если в локальной и удалённой ветке будут модифицироваться одни и те же файлы -
возникнет конфликт слияния. Подробнее о конфликтах, а также о способах их разрешения можно
прочитать здесь - [Wiki](http://mogilev.awem.by/mediawiki/index.php/Разрешение_конфликтов_Git)

* `git rebase <Репозиторий>/<Ветка>`
Данным способом будут "сшиты" воедино локальная и удалённые ветки. Подробнее о различиях между rebase
 и merge можно прочитать здесь -  [Wiki](http://mogilev.awem.by/mediawiki/index.php/Git_Rebase)

* Альтернативно можно сделать **pull**. Данная команда аналогична применению последовательности команд **fetch** и **merge**:

`git pull`

<span style="color:orange"> Внимание! </span> Данной командой вы заберёте историю всех веток, однако не данные из них.
Применение команды **git pull** также может спровоцировать конфликты.

#### Среда TortoiseGit

В среде Tortoise в окне параметров клонирования указывается имя ветки (параметр Branch), а также глубина (параметр Depth)
<span style="color:red"> Внимание! </span> В случае,если не будет указана глубина, а только имя ветки
будет произведено клонирование всех веток репозитория.

![](/img/Branch/6.png)

#### Среда SmartGit
Среда SmartGit также поддерживает частичное клонирование.

![](/img/Branch/7.png)

#### Среда SourceTree
В окне параметров клонирования можно задать необходимую глубину.

![](/img/Branch/8.png)

### <span style="color:blue"> <center>Выводы:</center> </span>
1. Команда **clone** используется для первого получения веток с удалённого сервера, команда **fetch** - для последующих получений;
2. Команды **clone** и **fetch** имеют параметр глубины, который позволяет задавать количество коммитов,
получаемых с сервера. Данный параметр можно задавать в графических средах для работы с Git и командном интерфейсе;
3. Влить полученные изменения в локальный репозиторий можно с помощью команд **merge** и **rebase**.
