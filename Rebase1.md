Git представляет два основных способа **применения изменений** из одной ветки в другую: *merge* и *rebase*.
Нагляднее всего разница между этими командами видна на приведённых ниже картинках:

![](/img/Rebase/1.png)

![](/img/Rebase/2.png)

**Merge** сравним с пожарной лестницей (ветка master), к концу которой крепятся все коммиты ветки devel.
При это devel остаётся самостоятельной веткой, к которой можно вернуться. **Rebase** - застёжка,
которая сшивает коммиты, "растворяя" ветку devel в master.

#### Выполнение rebase
Первоначально необходимо перейти в ветку, в которую вы хотите получить коммиты:

`git checkout feature`

![](/img/Rebase/3.png)

Выполните операцию **rebase**. Ветка master - ветка, с которой происходит получение коммитов:

`git rebase master`

![](/img/Rebase/4.png)

#### Отмена rebase

Для отмены воспользуйтесь следующей командой:

`git rebase --abort`

#### Продолжение rebase
В случае возникновения конфликтов их необходимо будет разрешить, а затем продолжить выполнение rebase:

`git rebase --continue`

#### Зачем используется rebase?
Для получения более аккуратной, линейной, а не разветвлённой истории.
Всё будет выглядеть так, как будто работа велась последовательно, но в действительно она
велась параллельно.

#### Демонстрация Rebase

* Создаем чистый репозиторий. Переключаемся на ветку master, добавляем файл InitFile, совершаем коммит и пуш.

![](/img/Rebase/5.png)

Посмотрим на историю коммитов:

![](/img/Rebase/6.png)

* Создаем ветку newBranch из master, добавляем файлы File1-File3. После каждого сообщения
делаем коммит. История коммитов выглядит следующим образом:

![](/img/Rebase/7.png)

* Переключаемся на ветку master, добавляем файл File4, делаем коммит. История коммитов при
этом начинает ветвиться:

![](/img/Rebase/8.png)

* Выполняем Rebase. Для этого переключаемся в ветку newBranch, указываем, что хотим получить
коммиты из ветки master. Как видно, теперь в ветке newBranch находится файл File4, который был в ветке master:

![](/img/Rebase/9.png)

Ветка master при этом не изменилась:

![](/img/Rebase/10.png)

Посмотрим на историю коммитов. Как видно, теперь она линейная:

![](/img/Rebase/11.png)

#### Конфликт при выполнении Rebase

В случае, когда выполняется попытка объединения коммитов, модифицирующих один и тот же файл
возникнет конфликт.
Воссоздадим ситуацию конфликта. Для этого сначала изменим файл File4 в ветке master и запушим изменения.

File4.txt
```txt
Mod
```

Теперь добавим файл с таким же названием в ветку newBranch, внесем изменения в данный файл и также запушим.

```txt
ModNewBranch
```

Посмотрим на историю коммитов:

![](/img/Rebase/14.png)

Попробуем совершить перемещение и получим сообщение о конфликте:

File4.txt
```
<<<<<< HEAD
Mod
=======
ModNewBranch
>>>>>> Modified File4
```
```bash
$ git rebase master
First, rewinding head to replay your work on top of it...
Applying: Add File1
Applying: Add File2
Applying: Add File3
Applying: Modified File4
Using index info to reconstruct a base tree...
Falling back to patching base and 3-way merge...
Auto-merging File4.txt
CONFLICT (add/add): Merge conflict in File4.txt
Patch failed at 0004 Modified File4
The copy of the patch that failed is found in: .git/rebase-apply/patch

When you have resolved this problem, run "git rebase —continue".
If you prefer to skip this patch, run "git rebase —skip" instead.
To check out the original branch and stop rebasing, run "git rebase —abort".
```

Вручную разрешим конфликт и продолжим rebase:

File4.txt
```text
Mod
```

```bash
$ git add File4.txt

$ git rebase —continue
Applying: Modified File4
```

#### Pull --rebase

Команда **pull** предназначена для обновления локального репозитория в соответствии с удаленным.
Данная команда поддерживает флаг **--rebase**. Продемонстрируем это.
Представим, что над репозиторием работают два разработчика. Они добавили себе адрес
удаленного репозитория. Первый разработчик добавил три файла и сделал три коммита, но не пушил их на сервер.
Его история выглядит следующим образом:

![](/img/Rebase/17.png)

Второй разработчик добавил один файл, сделал коммит и запушил его на сервер.
Первый разработчик хочет получить актуальную версию ветки. Для этого он сначала выполняет **fetch**, а затем команду **pull** с флагом *rebase*:

```bash
$ git fetch
remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
warning: no common commits
Unpacking objects: 100% (3/3), done.
From https://bitbucket.org/avatarl870/git_pull_rebase
 * [new branch]	master -> origin/master

$ git pull --rebase
First, rewinding head to replay your work on top of it...
Applying: Add File1
Applying: Add File2
Applying: Add File3
```

Теперь его история выглядит таким образом, что незапушенные изменения находятся впереди
полученных с сервера. При это нет лишней информации о мерже:

![](/img/Rebase/19.png)

Рассмотрим аналогичную ситуацию, но теперь первый разработчик выполняет обычный pull:

```bash
$ git pull origin master --allow-unrelated-histories
remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
warning: no common commits
Unpacking objects: 100% (3/3), done.
From https://bitbucket.org/avatarl870/git_rebase_pull
 *	branch	master	-> FETCH_HEAD
 *	[new branch]	master	-> origin/master
Merge made by the 'recursive' strategy.
 File4.txt | 0
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 File4.txt
```

История в таком случае отображает мерж локальных и удалённых изменений:

![](/img/Rebase/21.png)

#### Отличие происходящего при *merge* и *rebase*

Рассмотрим подробнее, что выполняет git при выполнении операций **merge** и **rebase**

Наша история разделилась на две ветки: *master* и *experimental*:

![](/img/Rebase/22.png)

В самом простом случае слияние произойдёт следующим образом: будет создан новый коммит на основе трех: по последнему коммиту из каждой ветки(C3 из experimental и C4 из master) и общий коммит предок для двух веток (C2).
В итоге история будет выглядеть следующим образом:

![](/img/Rebase/23.png)

Перемещение работает немного иначе: для двух веток находится общий предок, для каждого коммита в текущей ветке берется его дельта и сохрняется во временный файл, указатель текущей ветки устанавливается на тот же коммит, что и ветка, на которую выполняется перемещение и в конце последовательно применяются изменения:

![](/img/Rebase/24.png)


<span style="color:red"> <b> Важно!!! </b> </span> Недопустимо перемещать коммиты, которые существует вне вашего репозитория.
При выполнении **rebase** фактически удаляются старые коммиты и создаются новые, которые в общем-то
идентичны старым, но не во всем. Команда **rebase** переписывает SHA (id) коммитов, так что другие пользователя просто не смогут использовать команду **pull**.
Рассмотрим подробнее. Вы клонировали себе данные из удалённого
репозитория:

![](/img/Rebase/25.png)

И совершили в нём некотурую работу:

![](/img/Rebase/26.png)

В это время ваш коллега так же совершает работу(которая включает в себя и слияние) и пушит свои изменения.
История на удалённом сервере теперь выглядит следующим образом:

![](/img/Rebase/27.png)

Вы получаете себе эти изменения и сливаете их со своими наработками:

![](/img/Rebase/28.png)

Далее ваш коллега принимает решение использовать вместо слияния перемещение. Для того, чтобы теперь он мог отправить свои изменения на удалённый сервер
ему приходится использовать push --force, а данная команда перезаписывает историю на сервере:

![](/img/Rebase/29.png)

Вы получаете эти изменения, включающие в себя и новые коммиты:

![](/img/Rebase/30.png)

Теперь вам снова необходимо выполнить объединение, даже если оно было произведено до этого, т.к. перемещение изменило SHA коммитов и git считает их код новые.
Хотя на самом деле у вас уже есть наработки из C4 в своей истории:

 ![](/img/Rebase/31.png)

 Однако вы будете вынуждены объединить эту работу со своей, что в будущем иметь возможность работать с другими разработчиками.
 После того, как вы это сделаете, ваша история коммитов будет содержать два коммита C4 и C4` с одинаковыми изменениями и сообщениями,
 однако разными SHA. Так же данные коммиты отображаются в истории, что будет сбивать с толку. Когда вы захотите отправить изменения на сервер, туда попадут и дублирующиеся коммиты, что будет запутывать других разработчиков.


### <span style="color:blue"> <center>Выводы:</center> </span>
1. Флаг *--rebase* команды **pull** позволяет избежать отображения ненужной информации, относящейся к слиянию веток;

2. Команда **rebase** опаснее команды **merge** - "отребейзенная" локально ветка попадёт в
удалённый репозиторий с использованием push --force, что приведет к перезаписи истории на
сервере. Попытка слияния локальной и удаленной перезаписанной ("отребейзенной") ветки
приведет к непредсказуемым последствиям, вроде конфликтов либо появления дублирующихся
коммитов. Из этого следует следующее:

а) Нельзя ребейзить ничего в стабильных ветках (master, stable, production, release)
б) если ваш коммит попал в удалённый репозиторий
в) никогда нельзя ребейзить чужие коммиты  


3. Команда rebase предназначена для получения "красивой" линейной истории, она помогает избежать подобного:

![](/img/Rebase/32.png)
